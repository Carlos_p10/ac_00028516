        org     100h
        ;Ejer 1 28516
        mov     ax, 0000h ;limpiar ax
        mov     cx, 0000h ;limpiar cx
        mov     dx, 0000h; limpiamos dx
        add     cx, 2d
        add     cx, 8d
        add     cx, 5d
        add     cx, 1d
        add     cx, 6d ;2+8+5+1+6 = 22 
        mov     ax, cx ; ax = cx
        mov     bx, 5d ; 22/5 = 4.4 aprox_  = 4 
        div     bx ;la respuesta(cociente) se almacena en ax 

        mov     cl, "M"
        mov [200h], cl
        mov     cl, "e"
        mov [201h], cl
        mov     cl, "-"
        mov [202h], cl

        mov     cl, "r"
        mov [210h], cl
        mov     cl, "e"
        mov [211h], cl
        mov     cl, "c"
        mov [212h], cl        
        mov     cl, "u"
        mov [213h], cl
        mov     cl, "p"     
        mov [214h], cl
        mov     cl, "e"
        mov [215h], cl
        mov     cl, "r"
        mov [216h], cl
        mov     cl, "o"
        mov [217h], cl ;practicamente solo se pasa la letra a cada memoria asignada

    ;Ejer 2
        mov     ax, 0000h ;set 0 por la basura que trae
        mov     al, 2d ;inicio de enfermos
        mov     bx, 210h ;memoria que se le settea
        mov     cx, 2d ;estimacion
estimacion:    mul     cx     ;se multiplica lo que tenga ax por cx(2)
        mov     [bx], ax ;la memoria se settea con el nuevo valor (multiplicacion)
        cmp     ah, 00h ;mientras sea menor a 6 continuara normal 
        ja      mayor ;si es mayor a 00 o a la septima posicion de problemas y por eso agrego esta funcion
        je      menor ;si es igual a 00 los ira agregando
mayor:    add     bx, 2h ;agrega a los que sean mayores que 255
        jmp     sig
menor:    add     bx, 1h ;agrega normal hasta los que son menores que 255
        jmp     sig
sig:    cmp     bx, 21Fh ;loop normal con la condicion que le indica hasta donde parar 
        jb      estimacion

        ;Ejer 3
        ;limpiar primero todas las variables de los resultados anteriores
        mov     al, 0d ;seteamos 0
        mov     [220h], al ;f(0)
        mov     al, 01 ;seteamos 1
        mov     [221h], al ;f(1)
        mov     bp, 220h ;bp = base pointer, guarda la direccion del inicio de pila
        mov	    bl, 2d ;indice = 2

fibonacci:  mov     al, [bp] ;f(n) pasa a esa direccion de memoria
            mov     dl, [bp+1];f(n+1) pasa a esa direccion de memoria
            add     al,dl ; al += f(n-2)
            inc     bp ;incrementamos en uno la memoria
            mov     [bp+1], al ; el nuevo resultado pasa a esta direccion
            inc 	bl ;incrementamos el indice
            cmp	    bl, 16d	;seguir bucle mientras indice sea inferior a 16d
            jb	    fibonacci ;si la condicion de arriba no llega a su fin el bucle continua

        int 10h;


        ;run with ------------- nasm -f bin tarea_2_00028516.asm -o tarea.com
        ;then --------------- dosbox .